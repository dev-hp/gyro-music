# Gyroscope Music analysis

## History

End of 1984, at my 18th birthday, I was allowed to use my saved money to buy a 'Schneider CPC 464', the German version of an Amstrad CPC. Later, around 1985, I got a Gyroscope tape. Blessed with 'relative' hearing and learning to play the double bass at school, I got annoyed by the game music. It really seems to get out of sync very quickly.

At this time, I had no PC, just the CPC464 (but with the color monitor). I did not even own a dot-matrix printer, so I copied the disassembly output from a typed-in disassembler program, originally made for the ZX Spectrum and printed in c't magazine, with pencil onto sheets of scrap paper. Lots of them. After evening dinner, I often sat with my parnets in front of the living-room TV, pack of paper in my lap, ruler and pencil, and segmented the assembly listing into routines and commented them. I was fascinated with the different tape-loading and writing routines, and with Gyroscope I got fascinated with this bug.

My preliminary analysis lead me to the suspicion that the reason for the diverging voices of the tunes was 'forgetting' the Attack and Release phases (look up ADSR for context, if you want to) as part of the duration of the individual notes. That would lead to a quite fast de-synchronization of the three channels.

## Background of current analysis

Today, in February 2021, we have immensely more powerful and affordable computers. The amount of memory and the high screen resolution make it much easier to explore an 8-bit Z80 assembly program. In 1985, 64kB of memory were all I had, and that included 16kB of Video RAM. Today a computer with 4 cores, 16GB RAM and Windows 10 or Ubuntu 20 is not special anymore.

Enter WinAPE20B2. Load game data from a 'disk'. Run the program and save a memory snapshot. Open the built-in debugger and disassemble the progrtam and explore the data tables in memory.

Over the past many years, I have done bits of the analysis, off and on, but did not have the focus to finish it. Covid-19 and a few retro gaming videos on YoutTube reminded me to come back and finish the job. More recently, over the past five years, I learnt to use JavaScript for Embedded System development work.

## Source data

In the RAM data snapshot I use for analysis, the music routines 'live' in the memory range from $7000 to $8342.

- They register a 'fast interrupt' (300Hz). The interrupt handler for this interrupt divides by 3 for a 'tick' interrupt of 100Hz.
- With a frequency of 100Hz, three subroutines are called, one each for Channel A, Channel C and Channel B.
- The music routines don't use the AY-3-8912 built-in envelope feature, but 'manually' shape the volume envelope for each channel in an Attack-Sustain-Release pattern.
- The music data also does not use the noise generator. The routines for Channels A and C have code to support this, Channel B uses the assigned control bit in the data stream to select one of two Attack/Release values.

Music data starts at $7626 and ends at $8342.

### Tone Period Table

- The first data table is a list of the 12-bit (AY-3-8912) 'Tone Period' values for sixty different notes - spanning 5 octaves of each 12 halftones each. The 'lowest' tone is C3 at ca. 130 Hz, the highest is for B7 at ca. 3906 Hz (1.2% within its 'real' value of 3951Hz).

### Tune Tables

- Then follows a succession of four in-game music tunes. Each tune has these sub-tables:

  - 16 bytes for attack count and attack volume delta, release count and release volume delta. 4 bytes for Channel A, 4 bytes for Channel C, and two settings with each 4 bytes for Channel B.
  - Tune data for Channel A
  - Tune data for Channel C
  - Tune data for Channel B

  The tune data is structured in two-byte chunks like this:

  - First byte:
    - if the first byte is $FF, then the note routine returns, this marks the end of this voice.
    - if the first byte is $FE, then the playback pointer jumps to the start of the tune, and immediately thisnote is played. This allows for seamless 'looping'.
    - if bit 7 is set, the 'tone' for this channel is turned off
    - if bit 6 is set, the 'tone' for this channel is turned on
    - the lower 6 bits from bit 5...0 (a number from 0 to 63) are multiplied by 4 to give the duration of the 'sustain' phase of the note to play (total note length is this plus attack and delay count).
  - Second Byte:
    - for Channels A and C, if bit 7 is set, the 'noise' for this channel is turned on (yes, different order than for the tone enable), for Channel B, if bit 7 is set, the first attack/release parameter set is used
    - for Channels A and C, if bit 6 is set, the 'noise' for this channel is turned off, for Channel B, if bit 6 is set, the second attack/release parameter set is used
    - the lower 6 bits from bit 5...0 (a number from 0 to 63) is the number of the 'period' table entry that defines the current tone frequency

The overall tone duration is

- the number of 'attack phase' steps (in each step, the delta value is added to the volume parameter)
- four times the 'sustain value' from the first byte
- the number of 'release phase' steps (in each step, the delta value is subtracted from the volume parameter)

During playback, for each note the volume register is initialized to '1', therefore it does not matter if the 'attack' phase and the 'release' phase don't end at the same volume level.

### First findings from looking at the data

When I computed tables for the three voices and printed them side-by-side (like a 'piano roll'), I could debunk my initial assumption that the release values were 'forgotten'. The values for the complete duration of each played note line up nicely (see the gy-analyze.html output on the JavaScript console).

The reason for the horrible deviation of the three voices must be something else. And there are a number of candidates we will investigate further. But first let's look at the tunes itself.

### Tunes

These are the Gyroscope tunes:

- Tune X -- this tune is not selected in the normal music start routine ($5F32..$5F55)
  which cycles through tunes 0..2 with each call, at game start, or in the main game loop ($4F10..$4F2D).
- Tune 0 -- tune played on the menu screen and in the first level
- Tune 1 -- tune played in the second level
- Tune 2 -- (did not make it to level three)
- Tune 3 -- a short melody (10 notes), maybe when the player gets a 'bonus'? This, too, cannot be selected in the start routine - at the beginning of the routine the last tune number is incremented, and if the number of the tune is '3', it is reset to '0', so only 0, 1, 2 are ever reached.

## Making Music

After a few rounds of disassembling and commenting the music routines, cutting them out, assembling them into a smaller block, and loading them into WinAPE and playing around with them, I decided to use JavaScript for manipulating and visualizing the data (mostly by printing tables).

But I already had the full sound data in form of JavaScript data tables. It would be nice to actually play it, and would make changes easier.

So I checked if it is possible to actually play the data in JavaScript in a Browser. And it is possible.

> Well, it would have been faster if AudioContext were better documented - typical for a lot of JavaScript
> implementations, working with the documentation alone does not give you much of an idea how it all works together.
> Luckily there are a few examples to learn from.

My JavaScript code is certainly not the most elegant, but it works. And it seemingly also does 'its intended job' of fixing the sound problem. The channels do not desynchronize anymore. Enjoy this preview!

## Search for the root cause

So we have a nice JavaScript implementation of a playback routine for the original Gyroscope music data, and we have the data itself. And we cannot observe the bug that started the investigation. Why?

There are a number of parts to check in the disassembled program:

- Looks like there is a missing POP DE statement in the routine at $7613 that actually calls the three 'play next note' subroutines. This would mean the old DE value is restored to HL, and the 'return' statement actually jumps old HL value. (If this is a genuine bug, then it's a wonder it does not crash the whole program.)

**WORK-IN-PROGRESS**

## Links

- https://www.mobygames.com/game/cpc/gyroscope/screenshots
